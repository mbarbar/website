---
title: Variables
---
<!-- vale off -->

Three types of variables can be defined in Inko:

1. Local variables
1. Instance attributes
1. Constants

All variables are defined using the `let` keyword:

```inko
let local_variable = 10
let @instance_attribute = 10
let Constant = 10
```

Notably, however, constants start with a capital letter, and instance attributes
start with an `@`.

Variables can not be reassigned by default. To allow this, we can define a
using `let mut` instead:

```inko
let mut number = 10

number = 5
```

Constants can never be reassigned, meaning the following is invalid:

```inko
let mut Number = 10
```

The type of a variable is inferred from the value that is assigned to it.
However, it is possible to specify a type explicitly, as long as the assigned
value is compatible with it, just like function arguments:

```inko
let number: Integer = 10
```
