---
title: Introduction
---
<!-- vale off -->

This chapter covers compiler specific topics, such as the default source
directories and how code is compiled. The compiler is called "inkoc" and is
referred to as such in this section.
