---
title: Instance attributes
---
<!-- vale off -->

Syntax:

```ebnf
attribute = '@', identifier;
```

Instance attributes start with a `@`, followed by everything that is valid for
an identifier.

Some examples:

1. `@foo`
1. `@_foo`
1. `@foo123`
1. `@_foo123`
1. `@foo_bar`
