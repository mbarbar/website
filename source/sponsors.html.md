---
title: Sponsors
created_at: 2019-07-02
layout: sponsors
---
<!-- vale off -->

To support the development of Inko, we use [Open
Collective](https://opencollective.com) to receive donations and cover hosting
expenses. If you want to support Inko's development, please consider donating
via Open Collective.

Users and organisations who donate via Open Collective will be displayed on this
page. Those who have chosen the "Sponsor" tier will also have their logo and a
link displayed on the homepage. This requires that you have set up a logo for
your user or organisation.

The data on this page is refreshed periodically, so it may take several days for
your name to show up after donating for the first time.
